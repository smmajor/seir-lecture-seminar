# SEIR Lectures

In this repository, you can find content shared in the weekly-ish SEIR lecture
seminar.

| Delivered | Topic |
|-|-|
| 2 April 2023 | [Recursion](./recursion) |
| 23 April 2023 | [Trees](./trees) |

