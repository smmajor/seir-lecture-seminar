---
title: Recursion
patat:
    wrap: true
    margins:
        left: 8
        right: 8
    images:
        backend: kitty
    eval:
        python:
            command: python - 2>&1 | tail -n 8 | sed 's/while getting the str of an object//'
            fragment: false
            replace: false
        javascript:
            command: node -
            fragment: false
            replace: false
    theme:
        codeBlock: [onRgb#181818]
        syntaxHighlighting:
            string: [dullGreen]
            decVal: [vividCyan]
            comment: [vividBlack]
            builtIn: [bold]
...

# Recursion for fun and profit

## The intent of this series

Take a deep dive into computer science topics with real-world examples

This will often take us into three different realms:

- Maths - ∫x
- Data structures and algorithms - 
- Computer architecture - 🖧

### The structure

1. I'm going to talk for about 30 minutes
1. Then, we can chat for about 30 minutes

## Where we're going

Today, we're talking about _recursion_.

### 30-minute lecture

- A little data structure
- A little computer architecture
- Computer architecture + bulit-in data structure = recursion
- Iteration vs. recursion

### 30-minute chat

- Answering your questions
- Solving some problems

### Why recursion is important

- Understanding recursion helps us understand how computers work
- It is difficult to write iteration in some cases
- Next week is about **trees** where recursion can be really handy

# A little data structure

## A review of the stack data structure

We are reviewing the _stack_, today, because _any_ lanaguage with functions
has a stack built into the way it runs your code.

## A review of the stack data structure

A basic _stack_ has two\* commands: **push** and **pop**.

```text
                  ┌──────────────────────┐                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  └──────────────────────┘                   
                            Stack
```

\* Some stack implementations have more

## A review of the stack data structure

A basic _stack_ has two commands: **push** and **pop**.

```text
                  ┌──────────────────────┐                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
      PUSH        │                      │                   
 "Hello, world!"  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  └──────────────────────┘                   
                            Stack
```

## A review of the stack data structure

A basic _stack_ has two commands: **push** and **pop**.

```text
                  ┌──────────────────────┐                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                  
                  │         TOP          │                   
                  │    "Hello, world!"   │                   
                  │                      │                   
                  └──────────────────────┘                   
                            Stack
```

## A review of the stack data structure

A basic _stack_ has two commands: **push** and **pop**.

```text
                  ┌──────────────────────┐                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
     PUSH         │                      │                   
   1,001,001      │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                   
                  │         TOP          │                   
                  │    "Hello, world!"   │                   
                  │                      │                   
                  └──────────────────────┘                   
                            Stack
```

## A review of the stack data structure

A basic _stack_ has two commands: **push** and **pop**.

```text
                  ┌──────────────────────┐                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                  
                  │         TOP          │                   
                  │       1,001,001      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                   
                  │    "Hello, world!"   │                   
                  │                      │                   
                  └──────────────────────┘                   
                            Stack
```

## A review of the stack data structure

A basic _stack_ has two commands: **push** and **pop**.

```text
                  ┌──────────────────────┐                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
     PUSH         │                      │                   
     true         │                      │                   
                  │                      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                  
                  │         TOP          │                   
                  │       1,001,001      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                   
                  │    "Hello, world!"   │                   
                  │                      │                   
                  └──────────────────────┘                   
                            Stack
```

## A review of the stack data structure

A basic _stack_ has two commands: **push** and **pop**.

```text
                  ┌──────────────────────┐                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                  
                  │         TOP          │                   
                  │         true         │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                  
                  │       1,001,001      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                   
                  │    "Hello, world!"   │                   
                  │                      │                   
                  └──────────────────────┘                   
                            Stack
```

## A review of the stack data structure

A basic _stack_ has two commands: **push** and **pop**.

```text
                  ┌──────────────────────┐                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │       POP         
                  │                      │       → true     
                  │                      │                 
                  │                      │                  
                  │                      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                  
                  │         TOP          │                  
                  │       1,001,001      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                   
                  │    "Hello, world!"   │                   
                  │                      │                   
                  └──────────────────────┘                   
                            Stack
```

## A review of the stack data structure

A basic _stack_ has two commands: **push** and **pop**.

```text
                  ┌──────────────────────┐                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │      
     PUSH         │                      │     
     false        │                      │                 
                  │                      │                  
                  │                      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                  
                  │         TOP          │                 
                  │       1,001,001      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                   
                  │    "Hello, world!"   │                   
                  │                      │                   
                  └──────────────────────┘                   
                            Stack
```

## A review of the stack data structure

A basic _stack_ has two commands: **push** and **pop**.

```text
                  ┌──────────────────────┐                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │      
                  │                      │     
                  ├──────────────────────┤
                  │                      │                  
                  │          TOP         │                   
                  │         false        │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                  
                  │       1,001,001      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                   
                  │    "Hello, world!"   │                   
                  │                      │                   
                  └──────────────────────┘                   
                            Stack
```

## A review of the stack data structure

A basic _stack_ has two commands: **push** and **pop**.

```text
                  ┌──────────────────────┐                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │                   
                  │                      │      
                  │                      │     
                  ├──────────────────────┤
                  │                      │                  
      Last        │          TOP         │     First         
       in         │         false        │      out          
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                  
                  │       1,001,001      │                   
                  │                      │                   
                  ├──────────────────────┤
                  │                      │                   
                  │    "Hello, world!"   │                   
                  │                      │                   
                  └──────────────────────┘                   
                            Stack
```

## A review of the stack data structure

A _stack_ is a **LIFO** data structure: last in, first out.

A basic _stack_ has two commands:

- **push** to put something on the top of the stack
- **pop** to remove something from the top of the stack

Questions?

# Computer architecture

## How does this run?

```javascript
function parseIntegerSafe(s) {
  let value = Number.parseInt(s);
  if (Number.isNaN(value)) {
    value = 0;
  }
  return value;
}

function addTwoNumbers(x, y) {
  x = x.trim();
  y = y.trim();
  let value = parseIntegerSafe(x);
  value += parseIntegerSafe(y);
  return value;
}

console.log(addTwoNumbers("3", "4"))
console.log(addTwoNumbers("3", "a"))
```

## An interactive session

Run this command to start Python in an interactive debugging environemnt

```sh
pudb adder.py
```

Source code in **adder.py**

```python
def parse_integer_safe(s: str) -> int:
    try:
        value = int(s)
    except:
        value = 0
    return value


def add_two_numbers(x: str, y: str) -> int:
    x = x.strip()  # Same as .trim() in JS
    y = y.strip()  # Same as .trim() in JS
    value = parse_integer_safe(x)
    value += parse_integer_safe(y)
    return value


x = "   3 "
y = " 4   "
print(x, "+", y, "=", end=" ")
print(add_two_numbers(x, y))

y = "  a  "
print(x, "+", y, "=", end=" ")
print(add_two_numbers(x, y))
```

## What happens with recursion?

Run this command to see a stack overflow

```sh
python stack_overflow.py
```

Run this command to start Python in an interactive debuggin environment

```sh
pudb stack_overflow.py
```

Source code in **stack_overflow.py**

```python
def infinite_recursion(x: int):
    print(x)
    infinite_recursion(x + 1)

infinite_recursion(0)
```

## Stack blown!

Unbounded recursion causes a stack overflow

We need a way to control it

# Recursive functions

## Designing a recursive function

To control our recursion, we need to ask three questions:

1. What is the input that has a known answer? This is the _base case_
1. When the input is not the base case and our code makes a _recursive call_,
   what is passed to it?
1. How does the argument that we pass to the recursive call get closer to the
   _base case_?

## Three easy patterns for base cases

- If the input is 0
- If the input is an empty list
- If the input is the empty string

Sometimes we have to have _more than one base case_, but they often look like
the easy patterns in the list

# Putting it all together

## Summing the first n integers

**Problem statement:**  
Given an input n greater than or equal to 0, find the total of the numbers
from 0..n

```text
n = 0, return 0  
n = 1, return 0 + 1  
n = 2, return 0 + 1 + 2  
n = 3, return 0 + 1 + 2 + 3  
...   
n = k, return 0 + 1 + 2 + 3 + ... + k
```

### Now, we need to answer these questions:

1. What is the input that has a known answer? This is the _base case_
1. When the input is not the base case and our code makes a _recursive call_,
   what is passed to it?
1. How does the argument that we pass to the recursive call get closer to the
   _base case_?

## Summing the first n integers

**Problem statement:**  
Given an input n greater than or equal to 0, find the total of the numbers
from 0..n

```text
n = 0, return 0  
n = 1, return 0 + 1  
n = 2, return 0 + 1 + 2  
n = 3, return 0 + 1 + 2 + 3  
...   
n = k, return 0 + 1 + 2 + 3 + ... + k
```

### Now, we need to answer these questions:

1. What is the input that has a known answer? This is the _base case_
1. When the input is not the base case and our code makes a _recursive call_,
   what is passed to it?
1. How does the argument that we pass to the recursive call get closer to the
   _base case_?

### A trick!

When thinking about recursion, think of the cases backwards!

```text
n = k, return k + ... + 3 + 2 + 1 + 0
...   
n = 3, return           3 + 2 + 1 + 0  
n = 2, return               2 + 1 + 0  
n = 1, return                   1 + 0  
n = 0, return                       0  
```

Then, the last case is the _base case_!

## Summing the first n integers

**Problem statement:**  
Given an input n greater than or equal to 0, find the total of the numbers
from 0..n

```text
n = k, return k + ... + 3 + 2 + 1 + 0
...   
n = 3, return           3 + 2 + 1 + 0  
n = 2, return               2 + 1 + 0  
n = 1, return                   1 + 0  
n = 0, return                       0  
```

### Now, we need to answer these questions:

1. What is the input that has a known answer? This is the _base case_
1. When the input is not the base case and our code makes a _recursive call_,
   what is passed to it?
1. How does the argument that we pass to the recursive call get closer to the
   _base case_?

**Base case:**  
If the input is 0, return 0

**Recursive case:**  
Return the input number plus the recursive call of the number minus 1

**How does the argument get closer to the base case?**  
The number that we pass to the recursive call is one less than the number that
we receive which will evenutally make it all the way down to zero

## Checking if a string is a palindrome

**Problem statement:**  
Given an input string, determine if it is a palindrome (strings of zero length
are palindromes)

### Now, we need to answer these questions:

1. What is the input that has a known answer? This is the _base case_
1. When the input is not the base case and our code makes a _recursive call_,
   what is passed to it?
1. How does the argument that we pass to the recursive call get closer to the
   _base case_?

## Checking if a string is a palindrome

**Problem statement:**  
Given an input string, determine if it is a palindrome (strings of zero length
are palindromes)

### Now, we need to answer these questions:

1. What is the input that has a known answer? This is the _base case_
1. When the input is not the base case and our code makes a _recursive call_,
   what is passed to it?
1. How does the argument that we pass to the recursive call get closer to the
   _base case_?

Think like a human! If given this string, how would you check to see if it is a
palindrome?

```text
LHKXWARUROSGGHEAVGSQYJJGGHGMEOIWEXBBXEWIOEMGHGGJJYQSGVAEHGGSORURAWXKHL
```

## Checking if a string is a palindrome

**Problem statement:**  
Given an input string, determine if it is a palindrome (strings of zero length
are palindromes)

### Now, we need to answer these questions:

1. What is the input that has a known answer? This is the _base case_
1. When the input is not the base case and our code makes a _recursive call_,
   what is passed to it?
1. How does the argument that we pass to the recursive call get closer to the
   _base case_?

Think like a human! If given this string, how would you check to see if it is a
palindrome?

```text
LHKXWARUROSGGHEAVGSQYJJGGHGMEOIWEXBBXEWIOEMGHGGJJYQSGVAEHGGSORURAWXKHL
```

### A human algorithm

1. Check if the first and last characters of the string are the same
1. If they are not, stop because it is not a palindrome
1. Check if the second and second to last characters of the string are the same
1. If they are not, stop because it is not a palindrome
1. Check if the third and third to last characters ...
1. ...
1. Eventually, I have zero or one characters, which makes it **true**!

```text
LHKXWARUROSGGHEAVGSQYJJGGHGMEOIWEXBBXEWIOEMGHGGJJYQSGVAEHGGSORURAWXKHL
 HKXWARUROSGGHEAVGSQYJJGGHGMEOIWEXBBXEWIOEMGHGGJJYQSGVAEHGGSORURAWXKH
  KXWARUROSGGHEAVGSQYJJGGHGMEOIWEXBBXEWIOEMGHGGJJYQSGVAEHGGSORURAWXK
                                 ....
                                 XBBX
                                  BB

                                      → true!
```

## Checking if a string is a palindrome

**Problem statement:**  
Given an input string, determine if it is a palindrome (strings of zero length
are palindromes)

### Now, we need to answer these questions:

1. What is the input that has a known answer? This is the _base case_
1. When the input is not the base case and our code makes a _recursive call_,
   what is passed to it?
1. How does the argument that we pass to the recursive call get closer to the
   _base case_?

Think like a human! If given this string, how would you check to see if it is a
palindrome?

```text
LHKXWARUROSGGHEAVGSQYJJGGHGMEOIWEXBBXEWIOEMGHGGJJYQSGVAEHGGSORURAWXKHL
```

### A recursive algorithm

1. If the string is 0 or 1 characters long, return **true**
1. Otherwise, check if the first and last characters of the string are the same
1. If they are not the same, return **false**
1. If they are the same, return the palindrome check of the string without the
   first and last characters

**Base case:**  
Zero or one characters, return true

**Recursive case:**  
The string to check the first and last characters

**How does the argument get closer to the base case?**  
The string keeps getting shorter (by two characters) each time we make a
recursive call

## Summing numbers in a list

**Problem statement:**  
Given a list of numbers, find the total

## Summing numbers in a list

**Problem statement:**  
Given a list of numbers, find the total

**Base case:**  
If the list of numbers is empty, return 0

**What is passed to the recursive call?**  
The list of numbers to add up

**How does the argument get closer to the base case?**  
The number of numbers in the list gets shorter by one each time
we call the function until it is an empty list

## Summing numbers

**Problem statement:**  
Given a list of numbers, find the total

## Summing numbers

**Problem statement:**  
Given a list of numbers, find the total

```text
[1]         = 1
[1, 3]      = 1 + 3       = 4
[1, 3, 201] = 1 + 3 + 201 = 205
```

## Summing numbers

**Problem statement:**  
Given a list of numbers, find the total

```text
[1]         = 1
[1, 3]      = 1 + 3       = 4
[1, 3, 201] = 1 + 3 + 201 = 205
```

Use the backwards trick!

```text
[1, 3, 201] = 1 + 3 + 201 = 205
[1, 3]      = 1 + 3       = 4
[1]         = 1
```

## Summing numbers

**Problem statement:**  
Given a list of numbers, find the total

```text
[1]         = 1
[1, 3]      = 1 + 3       = 4
[1, 3, 201] = 1 + 3 + 201 = 205
```

Use the backwards trick!

```text
[1, 3, 201] = 1 + 3 + 201 = 205
[1, 3]      = 1 + 3       = 4
[1]         = 1
```

**Base case:**  
If the list of numbers is contains only one number, return that number

**What is passed to the recursive call?**  
The list of numbers to add up

**How does the argument get closer to the base case?**  
The number of numbers in the list gets shorter by one each time we call the
function until it is an empty list

# Questions

# More examples

## Magic fill

**Problem statement:**  
Given an "image" and a starting "pixel", replace all of the surrounding pixels
_of the same "color"_ with a new one

```text
#####.....#####......#####.
#....#...#.....#....#.....#
#####....#.....#....#.....#
#....#...#.....#....#.....#
#####.....#####......#####.
```

Starting at "pixel" (11, 1), fill all pixels of the same color with "\*".

```text
#####.....#####......#####.
#....#...#.X...#....#.....#
#####....#.....#....#.....#
#....#...#.....#....#.....#
#####.....#####......#####.
```

## Magic fill

**Problem statement:**  
Given an "image" and a starting "pixel", replace all of the surrounding pixels
_of the same "color"_ with a new one

```text
#####.....#####......#####.
#....#...#.....#....#.....#
#####....#.....#....#.....#
#....#...#.....#....#.....#
#####.....#####......#####.
```

Starting where the "X" is, it spreads "out" and replaces the "X" with a "\*".

```text
#####.....#####......#####.
#....#...#X*X..#....#.....#
#####....#.X...#....#.....#
#....#...#.....#....#.....#
#####.....#####......#####.
```

## Magic fill

**Problem statement:**  
Given an "image" and a starting "pixel", replace all of the surrounding pixels
_of the same "color"_ with a new one

```text
#####.....#####......#####.
#....#...#.....#....#.....#
#####....#.....#....#.....#
#....#...#.....#....#.....#
#####.....#####......#####.
```

It keeps going until it repaces all of the "." pixels with "\*" pixels inside
the "#" pixels.

```text
#####.....#####......#####.
#....#...#*****#....#.....#
#####....#*****#....#.....#
#....#...#*****#....#.....#
#####.....#####......#####.
```

